<?php

namespace StuffAndyMakes\ConsoleHelpers;

/* 
 * CS - Color String Class; Makes it quick and easy to output color to the terminal for scripting.
 * Usage: CS::Red("Error!") returns string "Error!" in red, no background color settings.
 *   CS::RedOnCyan("Yikes!") returns string "Yikes!" in red over cyan background.
 * Note: Strings are terminated with color reset.
 * Credits: Color codes were found at https://gist.github.com/chrisopedia/8754917
 */
class CS {

    private static $codesByName = array(
		# Attributes
		'Reset' => '0',
		'Bright' => '1',
		'Bold' => '1',
		'Bolded' => '1',
		'Emphasized' => '1',
		'Dim' => '2',
		'Light' => '2',
		'Faded' => '2',
		'Underscore' => '4',
		'Underscored' => '4',
		'Underline' => '4',
		'Underlined' => '4',
		'Blink' => '5',
		'Blinking' => '5',
		'Reverse' => '7',
		'Reversed' => '7',
		'Hidden' => '8',
		'Invisible' => '8',
    	# Foreground colors
		'Black' => '30',
		'Red' => '31',
		'Green' => '32',
		'Yellow' => '33',
		'Blue' => '34',
		'Magenta' => '35',
		'Purple' => '35',
		'Cyan' => '36',
		'White' => '37',
		# Background colors
		'OnBlack' => '40',
		'OnRed' => '41',
		'OnGreen' => '42',
		'OnYellow' => '43',
		'OnBlue' => '44',
		'OnMagenta' => '45',
		'OnPurple' => '45',
		'OnCyan' => '46',
		'OnWhite' => '47'
	);

    private function __construct() {}

	private static function getColorWords(string $methodName = 'Reset') {
	}

	/*
	 *  This method "intercepts all method calls on this class and is what allows
	 *  this class to "fake" methods for color names by mapping known names from 
	 *  the arrays above and combinations of names, like RedOnWhite(), for example.
	 *  (Because how dumb is it to write a method for each combination of colors? amirite??)
	 */
    public static function __callStatic( $methodName, $arguments ) {
    	if( count( $arguments ) < 1 ) {
			return $arguments[0]; // can't figure out what they want, so output string uncolored
		}
		if (strlen($methodName) < 1) $methodName = 'Reset';
		// first, break up the imaginary method name into capitalized words
		$words = [];
		$startOfWord = 0;
		for ($c = 1; $c < strlen($methodName); $c++) { 
			if ($methodName[$c] >= 'A' && $methodName[$c] <= 'Z') {
				array_push($words, substr($methodName, $startOfWord, $c - $startOfWord));
				$startOfWord = $c;
			}
		}
		array_push($words, substr($methodName, $startOfWord, $c - $startOfWord));
		$codes = '';
		$codeName = '';
		while (count($words) > 0) {
			if ($words[0] == 'On') {
				// this is for background colors (e.g. "OnBlue")
				array_shift($words); // shift off the "On"
				$codeName = 'On' . array_shift($words); // shift off the color (supposed to be, anyway)
			} else {
				$codeName = array_shift($words);
			}
			// if there is such a code, add it to the command list
			if (array_key_exists($codeName, self::$codesByName)) {
				$codes .= (strlen($codes) > 0 ? ';' : '') . self::$codesByName[$codeName];
			}
		}
		// return Color Codes + Message + Reset Color Code
		return "\e[" . $codes . 'm' . $arguments[0] . "\e[0m";
	}

}
