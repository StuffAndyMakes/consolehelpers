# ConsoleHelpers

## Description

Simple PHP classes for making output in the terminal easier, including color.

There are libraries out there that do what this does, but I couldn't find one that was specifically for simple output, had a couple of easy input functions, some canned messaging functions, and super-slim, easy-to-use output coloring.

Output to the terminal is done through the `Console` class. Coloring output inline is done with the `CS` class through its only method that has a zillion different names and the names determine the attributes of the string.

## Usage

I tried to make this thing as quick and simple as possible, so lemme show you.

The `CS` class is meant to be called statically everywhere. It's only named with two characters to save typing. Calls to it for colored output are meant to go inline in a string or print function. You'll see its usage below.

### Console - Output helper class

Let's start with the `Console` class, first.

Get a `Console` class object going and optionally clear the screen:
```php
$con = new Console(true); // get a console object and pass true to clear screen
        // Console(); // don't clear screen
```

Simple output with newline termination:
```php
$con->println('Hello, world!');
```

Or, don't want a newline? Do this:
```php
$con->print('Not gonna go to the new line...');
```

There's a little helper constant in the class if you want to newline the hard way:
```php
$con->print('Another way to add a newline.' . $con::NL);
```

Need to output to STDERR? I know I do! Here's how:
```php
$con->errorln('This will output to STDERR with a newline.');
$con->error('This goes to STDERR without a newline.');
$con->error("\n"); // terminates it with a newline the old fashioned way
```

Maybe you don't find yourself needing time-saving OK, Error, FYI style colored messaging, but you get it with this deal:
```php
$con->OK('This is an OK message!');
$con->ERR('This is an ERR message. :(');
$con->YES('This is an YES message!');
$con->NO('This is an NO message!');
$con->FYI('This is an FYI message.');
```

![ConsoleHelpers colored notifications](images/notifications.png "Colored notifications")

### CS - The output coloring thing

The CS class has a single method and answers calls to any combination of the words listed below. The calls come into the `__callStatic()` magic function. It receives the name of the function as you named it in your code as the first parameter. The string you want to color is passed through the `arguments` array. When you call `CS::BrightYellowOnBlue('a string')`, for example, the method breaks its name apart by Capitalized words. So, based on our example, it would get `['Bright', 'Yellow', 'OnBlue']`. Then it finds the words as keys in the codes array and builds the ANSI color coding string. It prepends the color coding to the string to print and then appends the reset code (to put the text back to normal after the string) and returns the string.

Non-Coloring Attributes:

* `Reset`
* `Bright` (also `Bold`, `Bolded`, `Emphasized`)
* `Dim` (also `Light`, `Faded`)
* `Underscore` (also `Underscored`, `Underline`, `Underlined`)
* `Blink` (also `Blinking`)
* `Reverse` (also `Reversed`)
* `Hidden` (also `Invisible`)

Colors

* `Black`
* `Red`
* `Green`
* `Yellow`
* `Blue`
* `Magenta` (also `Purple`)
* `Cyan`
* `White`

_To make any color a background color, simply add `On` in front of the color. It needs to be capitalized, like all the other words listed above. Capitalization is how the function breaks apart the words._

How some color output examples? Here ya go...
```php
$con->print( CS::DimRed($sample) );
$con->print( CS::Red($sample) );
$con->print( CS::BoldRed($sample) );
$con->print( CS::UnderlinedRed($sample) );
$con->println( CS::BlinkingRed($sample) );
```

![ConsoleHelpers colored output](images/color-output.gif "Sample colored output")

The cool thing about the CS class is that it dynamically answers to your request for colors and attributes for the text you want to output. For example, the following calls are all the exact same thing:
```php
$con->println( CS::BoldUnderlinedYellowOnBlue('Sample Text') );
$con->println( CS::UnderlinedYellowBoldedOnBlue('Sample Text') );
$con->println( CS::YellowOnBlueEmphasizedUnderlined('Sample Text') );
```

## Installation

*Git it*... Replace `constuff` with whatever directory name you want for your project or for testing out this thing.

```
$ git clone git@bitbucket.org:StuffAndyMakes/consolehelpers.git constuff
```

Change into the directory...

```
$ cd constuff
```

Do the `composer` thang... Gets you a proper `vendor` directory.

```
$ composer dumpautoload -o
```

To walk through the examples, run the `examples.php` script:
```php
$ php examples.php
```
